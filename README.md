# nacos-springcloud

#### 介绍
​		基于nacos的分布式服务架构搭建，并通过云效或Teambition飞流的形式部署到阿里云Serverless应用引擎（SAE）中。

​        [Serverless应用引擎](https://help.aliyun.com/product/118957.html?spm=a2c4g.11186623.6.540.18d432deI9q0VB)（SAE）是面向应用的Serverless PaaS平台，帮助PaaS层用户免运维IaaS、按量计费、低门槛微服务上云，将Serverless架构和微服务架构的完美结合。

#### 软件架构
本项目是针对中小企业应用微服务简单的小型架构：

![简单系统架构](./media/简单系统架构.png)



#### 使用说明

   本项目应用nacos为服务注册与发现，运行本项目中的工程需先安装[Nacos Server](https://github.com/alibaba/nacos/releases)，启动了Nacos Server后可将工程跑起来。

​    注意：代码中用到 lombok插件，需在idea中安装lombok插件。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
