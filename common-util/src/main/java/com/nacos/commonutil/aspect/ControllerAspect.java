package com.nacos.commonutil.aspect;

import com.nacos.commonutil.context.LocalRequestContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: zlj
 * @Date: 2021/3/22 17:20
 * @Description:
 *      针对所有Controller的切片，打印访问参数与输出日志
 */
@Slf4j
@Aspect
@Component
public class ControllerAspect {
    /**
     * 记录起始时间，用于计算执行花费的时间
     */
    private final ThreadLocal<Long> startTime = new ThreadLocal<>();

    @Pointcut("execution(public * com.nacos.*.controller..*(..))")
    public void controllerPointCut(){}

    @Before("controllerPointCut()")
    public void beforeExcution(JoinPoint joinPoint){
        this.startTime.set(System.currentTimeMillis());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        Signature signature = joinPoint.getSignature();
        signature.getDeclaringType();
        HttpServletRequest request = attributes.getRequest();
        //设置上下文
        LocalRequestContextHolder.setLocalRequestContext(request, attributes.getResponse());
    }

    @After("controllerPointCut()")
    public void afterExecution(){

    }

    @AfterReturning(returning = "ret",pointcut = "controllerPointCut()")
    public void doAfterReturningControllerExecution(Object ret) {
        Long spend = System.currentTimeMillis() - startTime.get();
        log.info("API执行消耗：{} ms", spend);
        startTime.remove();
    }
}
