package com.nacos.commonutil.controller;

import com.nacos.commonutil.constant.CodeEnum;
import com.nacos.commonutil.vo.ResponseMsg;
import org.springframework.validation.BindingResult;

/**
 * @Author: zlj
 * @Date: 2021/3/22 17:24
 * @Description:
 *      统一公共返回模版Controller
 */
public class BaseController {

    /**
     * 请求成功返回（无参）
     *
     * @return
     */
    public ResponseMsg success() {
        return success(null);
    }

    /**
     * 请求成功返回
     *
     * @param data
     * @return
     */
    public <T> ResponseMsg success(T data) {
        ResponseMsg rd = new ResponseMsg();
        rd.setStatus(CodeEnum.SUCCESS.getCode());
        rd.setDesc(CodeEnum.SUCCESS.getMsg());
        rd.setBody(data);
        return rd;
    }

    /**
     * 失败返回
     * @return
     */
    public ResponseMsg fail(){
        ResponseMsg rd = new ResponseMsg();
        rd.setStatus(CodeEnum.FAIL.getCode());
        rd.setDesc(CodeEnum.FAIL.getMsg());
        return rd;
    }

    /**
     * 请求失败返回 （校验返回）
     */
    public ResponseMsg fail(BindingResult errors) {
        ResponseMsg rd = new ResponseMsg();
        rd.setStatus(CodeEnum.REQUEST_PARAM_ERROR.getCode());
        errors.getAllErrors().stream().forEach(error -> {
            rd.setDesc(error.getDefaultMessage());
        });
        return rd;
    }

}
