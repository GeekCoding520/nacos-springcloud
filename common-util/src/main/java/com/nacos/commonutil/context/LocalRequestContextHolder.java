package com.nacos.commonutil.context;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * @Author: liujian
 * @Date: 2019/10/24 11:06
 * @Description:
 *          上下文操作对象
 */
public class LocalRequestContextHolder {

    /**
     * 记录HttpServletRequest，用于记录请求信息
     */
    private final static ThreadLocal<LocalRequestContext> contexts = new ThreadLocal<>();

    private LocalRequestContextHolder() {
    }

    /**
     * 初始化当前请求的上下文
     *
     * @param httpServletRequest
     */
    public static void setLocalRequestContext(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        LocalRequestContext rc = new LocalRequestContext();
        rc.request = httpServletRequest;
        rc.session = httpServletRequest.getSession();
        rc.response = httpServletResponse;
        rc.cookies = new HashMap<>();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                rc.cookies.put(ck.getName(), ck);
            }
        }
        contexts.set(rc);
    }

    /**
     * 获取当前请求的上下文
     *
     * @return
     */
    public static LocalRequestContext getLocalRequestContext() {
        return contexts.get();
    }

    /**
     * 清除当前线程对请求上下文对象的引用（即让GC回收当前请求上下文对象）
     */
    public static void destoryLocalRequestContext() {
        contexts.remove();
    }

}
