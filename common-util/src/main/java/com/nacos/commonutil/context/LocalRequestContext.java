package com.nacos.commonutil.context;

import lombok.Data;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Author: liujian
 * @Date: 2019/10/24 11:05
 * @Description:
 *          当前请求上下文对象 (存放了HttpSession、HttpServletRequest等对象)
 */
@Data
public class LocalRequestContext {
    public ServletContext servletContext;
    public HttpSession session;
    public HttpServletRequest request;
    public HttpServletResponse response;
    public Map<String, Cookie> cookies;
}
