package com.nacos.zuul.filter;

import com.nacos.commonutil.constant.CodeEnum;
import com.nacos.commonutil.vo.ResponseMsg;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

/**
 * @Author: zlj
 * @Date: 2021/3/22 19:10
 * @Description:
 *
 */
@Slf4j
@Component
public class ErrorFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return FilterConstants.ERROR_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        log.info("error：处理请求时发生错误时被调用");
        ResponseMsg responseMsg = new ResponseMsg();
        responseMsg.setCodeEnum(CodeEnum.ROUTE_ERROR);
        return responseMsg;
    }
}
