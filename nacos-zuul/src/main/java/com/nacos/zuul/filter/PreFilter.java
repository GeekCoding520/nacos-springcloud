package com.nacos.zuul.filter;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.nacos.commonutil.constant.CodeEnum;
import com.nacos.commonutil.vo.ResponseMsg;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;

/**
 * @Author: zlj
 * @Date: 2021/3/22 19:07
 * @Description:
 *      pre过滤器，可以在请求被路由之前调用
 */
@Slf4j
@Component
public class PreFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        // 当shouldFilter返回 true时，下面run方法才会被调用
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        // 是否拦截
        Boolean isInterceptor = true;
        // 白名单api不拦截
        String requestURI = request.getRequestURI();


        ResponseMsg responseMsg = new ResponseMsg();
        if (isInterceptor){
            responseMsg.setStatus(CodeEnum.API_BEING_INTERCEPTED.getCode());
            String msg = String.format(CodeEnum.API_BEING_INTERCEPTED.getMsg(),request.getServletPath());
            responseMsg.setDesc(msg);

            log.info(msg);
            try {
                ctx.setResponseStatusCode(HttpStatus.OK.value());
                ctx.addZuulResponseHeader("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
                ctx.setResponseBody(JSONUtils.serializeObject(responseMsg));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        //白名单拦截
        return null;
    }
}
