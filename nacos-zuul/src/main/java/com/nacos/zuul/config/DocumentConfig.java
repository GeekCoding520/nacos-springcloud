package com.nacos.zuul.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: liujian
 * @Date: 2019/10/25 14:43
 * @Description: 分布式文档配置
 */
@Component
@Primary
@Slf4j
public class DocumentConfig implements SwaggerResourcesProvider {

    @Value("${swagger.title}")
    private String title;

    @Value("${swagger.version}")
    private String version;

    @Autowired
    RouteLocator routeLocator;

    @Override
    public List<SwaggerResource> get() {
        //获取所有router
        List<SwaggerResource> resources = new ArrayList<>();
        routeLocator.getRoutes().forEach(x ->  resources.add(swaggerResource(x.getId(), x.getFullPath().replace("/**", "") + "/v2/api-docs", version)));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
