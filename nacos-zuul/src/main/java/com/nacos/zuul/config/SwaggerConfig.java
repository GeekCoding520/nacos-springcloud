package com.nacos.zuul.config;

import com.nacos.commonutil.constant.CodeEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import springfox.documentation.builders.*;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * @Author: liujian
 * @Date: 2019/10/24 17:35
 * @Description: Swagger-api 文档配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig{

    @Value("${swagger.title}")
    private String title;

    @Value("${swagger.version}")
    private String version;

    // 可以注入多个doket，也就是多个版本的api
    @Bean
    @Order(value = 1)
    public Docket createRestApi() {

        // 添加错误响应码
        List<ResponseMessage> responseMessageList = CodeEnum.parseErrorCode();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title(title).version(version).build());

    }
}
