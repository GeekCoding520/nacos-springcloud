package com.nacos.zuul.fallback;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.nacos.commonutil.constant.CodeEnum;
import com.nacos.commonutil.vo.ResponseMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.*;

/**
 * @Author: zlj
 * @Date: 2021/3/22 19:08
 * @Description:
 *      Zuul的错误回调
 */
@Slf4j
@Component
public class FeignFallbackProvider implements FallbackProvider {

    @Override
    public String getRoute() {
        // serviceId，如果需要所有的调用都支持回退，则return "*" 或 return null
        return "*";
    }

    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        log.info("fallbackRepsonse: " + route);
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.OK.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.OK.getReasonPhrase();
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() throws IOException {
                // 自定义回调失败body
                ResponseMsg responseMsg = new ResponseMsg();
                responseMsg.setCodeEnum(CodeEnum.ROUTE_ERROR);
                ByteArrayOutputStream byt=new ByteArrayOutputStream();
                ObjectOutputStream obj=new ObjectOutputStream(byt);
                obj.writeObject(responseMsg);
                return new ByteArrayInputStream(JSONUtils.serializeObject(responseMsg).getBytes());
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                return headers;
            }
        };
    }
}
